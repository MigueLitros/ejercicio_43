package dominio;

import java.util.ArrayList;

public class Provincia {
	
	private String nombre;
	private ArrayList<Municipio> municipios = new ArrayList<>();

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void addMunicipio(Municipio municipio) {
        	municipios.add(municipio);
    	}

	public ArrayList<Municipio> getMunicipios(){
		return municipios;
	}

	public int calcularNumeroHabitantes() {
		int numeroHabitantes = 0;
		for(Municipio municipio : municipios) {
			numeroHabitantes += municipio.calcularNumeroHabitantes();
		}
		return numeroHabitantes;
	}
	
	public String toString(){
		return "Nombre de la provincia: " + nombre + "\nNumero de habitantes en la provincia: " + calcularNumeroHabitantes();
	}
}
