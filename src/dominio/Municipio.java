package dominio;

import java.util.ArrayList;

public class Municipio {
	private String nombre;
	private ArrayList<Localidad> localidades = new ArrayList<>();

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void addLocalidad(Localidad localidad) {
        localidades.add(localidad);
    }
    public ArrayList<Localidad> getLocalidades() {
        return localidades;
    }
	public int calcularNumeroHabitantes() {
		int numeroHabitantes = 0;
		for(Localidad localidad : localidades) {
			numeroHabitantes += localidad.getNumeroDeHabitantes();
		}
		return numeroHabitantes;
	}
	public String toString() {
		return "Nombre: " + nombre + "\nNumero de habitantes por localidad: " + calcularNumeroHabitantes();
	}
}
