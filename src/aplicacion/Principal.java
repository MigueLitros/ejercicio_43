package aplicacion;

import dominio.Localidad;
import dominio.Municipio;
import dominio.Provincia;

public class Principal {

	public static void main(String[] args) {

		Provincia provincia = new Provincia();
    	provincia.setNombre("Mi Provincia");

    	for (int i = 1; i <= 2; i++) {
        	//Creamos un Municipio.
        	Municipio municipio = new Municipio();
        	municipio.setNombre("Mi municipio_"+i);

        	for (int j = 1; j <= 3; j++) {
            	//Creamos localidad.
            	Localidad localidad = new Localidad();
            	localidad.setNombre("Mi Localidad_"+i+"_"+j);
            	localidad.setNumeroDeHabitantes(10000);

            	//Añadimos localidad al municipio.
            	municipio.addLocalidad(localidad);

            	//Repetimos esto tres veces. (añadimos 3 localidades).
        	}

       		//Añadimos municipio a la localidad.
        	provincia.addMunicipio(municipio);

        	//Repetimos esto dos veces. (añadimos 2 municipios (con 3 localidades cada uno) a la provincia).
    	}
    	System.out.println("\n");
		System.out.println(provincia.toString());
		System.out.println("\n");
		for (Municipio municipio : provincia.getMunicipios()) {

  			System.out.println("Nombre municipio: " + municipio.getNombre() + ".\nLocalidades: ");

  			for (Localidad localidad : municipio.getLocalidades()) {

      			System.out.println("\nNombre: " + localidad.getNombre() + ".\nNumero habitantes: " + localidad.getNumeroDeHabitantes() + ".\n");
  			}
		}
	}
}